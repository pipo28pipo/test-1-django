from django.shortcuts import render
import cgi
# Create your views here.
def home(request):
    return render(request,'home.html',None)
def result(request):
    sign = request.GET['sign']
    v1 = request.GET['v1']
    v2 = request.GET['v2']
    if sign == 'plus':
       result = int(v1)+int(v2)
    elif sign == 'minus':
       result = int(v1)-int(v2)
    elif sign == 'div':
       result = float(v1)/float(v2)
    elif sign == 'times':
       result = float(v1)*float(v2)
    print(request.GET['v1'])
    context = {'result':result}
    return render(request,'result.html',context)
